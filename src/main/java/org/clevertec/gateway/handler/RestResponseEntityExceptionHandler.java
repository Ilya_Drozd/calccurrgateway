package org.clevertec.gateway.handler;


import lombok.RequiredArgsConstructor;
import org.clevertec.gateway.constants.MessageNames;
import org.clevertec.gateway.exception.BadInputDataException;
import org.clevertec.gateway.exception.ServerErrorException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(ServerErrorException.class)
    protected ResponseEntity<Object> handleServerErrorException(ServerErrorException e) {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadInputDataException.class)
    protected ResponseEntity<Object> handleBadInputDataException(BadInputDataException e) {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK), HttpStatus.BAD_REQUEST);
    }
}

