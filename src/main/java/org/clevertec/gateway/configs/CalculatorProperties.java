package org.clevertec.gateway.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(value = "services.calculator")
public class CalculatorProperties {
    private String scheme;
    private String baseInfo;
    private String calculatePath;
}
