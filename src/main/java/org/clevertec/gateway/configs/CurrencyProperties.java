package org.clevertec.gateway.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(value = "services.currency")
public class CurrencyProperties {
    private String scheme;
    private String baseInfo;
    private String conversionAmountPath;
    private String conversionRatePath;
    private String cultureInfoPath;
    private String currenciesPath;
    private String currencyRatePath;
    private String lastUpdateDatePath;
}
