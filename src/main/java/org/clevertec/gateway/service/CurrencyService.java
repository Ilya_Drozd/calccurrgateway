package org.clevertec.gateway.service;


import org.clevertec.gateway.dto.CurrencyConverterData;
import org.clevertec.gateway.dto.CurrencyData;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;

public interface CurrencyService {
    BigDecimal getConversionAmount(CurrencyConverterData currencyConverterData);
    BigDecimal getConversionRate(CurrencyConverterData currencyConverterData);
    String getCultureInfo(CurrencyData currencyData);
    List<String> getCurrencies();
    BigDecimal getCurrencyRate(CurrencyData currencyData);
    XMLGregorianCalendar getLastUpdateDate();
}
