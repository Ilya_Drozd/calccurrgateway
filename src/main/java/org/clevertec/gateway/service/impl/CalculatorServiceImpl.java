package org.clevertec.gateway.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.gateway.constants.MessageNames;
import org.clevertec.gateway.dto.CalculatorData;
import org.clevertec.gateway.exception.BadInputDataException;
import org.clevertec.gateway.exception.ServerErrorException;
import org.clevertec.gateway.repository.CalculatorRepository;
import org.clevertec.gateway.service.CalculatorService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Locale;


@Service
@Slf4j
@RequiredArgsConstructor
public class CalculatorServiceImpl implements CalculatorService {

    private final MessageSource messageSource;
    private final CalculatorRepository calculatorRepository;

    @Override
    public int calculate(CalculatorData calculatorData) {
        try {
            int result = calculatorRepository.calculate(calculatorData);

            log.info(messageSource.getMessage(MessageNames.SUCCESSFUL_CALCULATOR_OPERATION_MESSAGE,
                    new Object[]{calculatorData.getNumbers()[0],
                            calculatorData.getNumbers()[1],
                            calculatorData.getOperationType(),
                            result},
                    Locale.UK));
            return result;

        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        } catch (HttpClientErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new BadInputDataException(e.getResponseBodyAsString());
        }
    }
}
