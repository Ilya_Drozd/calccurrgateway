package org.clevertec.gateway.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.clevertec.gateway.constants.MessageNames;
import org.clevertec.gateway.dto.CurrencyConverterData;
import org.clevertec.gateway.dto.CurrencyData;
import org.clevertec.gateway.exception.BadInputDataException;
import org.clevertec.gateway.exception.ServerErrorException;
import org.clevertec.gateway.repository.CurrencyRepository;
import org.clevertec.gateway.service.CurrencyService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;


import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final MessageSource messageSource;

    @Override
    public BigDecimal getConversionAmount(CurrencyConverterData currencyConverterData) {
        try {
            BigDecimal result = currencyRepository.getConversionAmount(currencyConverterData);

            log.info(messageSource.getMessage(
                    MessageNames.CONVERSION_AMOUNT_MESSAGE,
                    new Object[]{currencyConverterData.getCurrencyFrom(),
                            currencyConverterData.getCurrencyTo(),
                            currencyConverterData.getAmount(),
                            currencyConverterData.getRateDate(),
                            result.toString()},
                    Locale.UK));

            return result;
        } catch (HttpClientErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new BadInputDataException(e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }
    }

    @Override
    public BigDecimal getConversionRate(CurrencyConverterData currencyConverterData) {
        try {
            BigDecimal result = currencyRepository.getConversionRate(currencyConverterData);

            log.info(messageSource.getMessage(
                    MessageNames.CONVERSION_RATE_MESSAGE,
                    new Object[]{currencyConverterData.getCurrencyFrom(),
                            currencyConverterData.getCurrencyTo(),
                            currencyConverterData.getRateDate(),
                            result.toString()},
                    Locale.UK));
            return result;
        } catch (HttpClientErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new BadInputDataException(e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }
    }

    @Override
    public String getCultureInfo(CurrencyData currencyData) {
        try {
            String result = currencyRepository.getCultureInfo(currencyData);
            log.info(messageSource.getMessage(MessageNames.CULTURE_INFO_MESSAGE,
                    new Object[]{currencyData.getCurrency(), result}, Locale.UK));
            return result;
        } catch (HttpClientErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new BadInputDataException(e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }
    }

    @Override
    public List<String> getCurrencies() {
        try{
            List<String> result = currencyRepository.getCurrencies();
            log.info(messageSource.getMessage(MessageNames.CURRENCIES_MESSAGE, new Object[]{result}, Locale.UK));
            return result;
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }

    }

    @Override
    public BigDecimal getCurrencyRate(CurrencyData currencyData) {
        try {
            BigDecimal result = currencyRepository.getCurrencyRate(currencyData);
            log.info(messageSource.getMessage(MessageNames.CURRENCY_RATE_MESSAGE,
                    new Object[]{
                            currencyData.getCurrency(),
                            currencyData.getRateDate(),
                            result.toString()},
                    Locale.UK));
            return result;
        } catch (HttpClientErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_INPUT_DATA_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new BadInputDataException(e.getResponseBodyAsString());
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }
    }

    @Override
    public XMLGregorianCalendar getLastUpdateDate() {
        try{
            XMLGregorianCalendar result = currencyRepository.getLastUpdateDate();
            log.info(messageSource.getMessage(MessageNames.LAST_UPDATE_DATE_MESSAGE, new Object[]{result}, Locale.UK));
            return result;
        } catch (HttpServerErrorException e) {
            log.error(messageSource.getMessage(MessageNames.ERROR_SERVER_MESSAGE,
                    new Object[]{e.getResponseBodyAsString()}, Locale.UK));
            throw new ServerErrorException(e.getResponseBodyAsString());
        }

    }
}
