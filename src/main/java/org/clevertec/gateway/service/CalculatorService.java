package org.clevertec.gateway.service;

import org.clevertec.gateway.dto.CalculatorData;

public interface CalculatorService {
    int calculate(CalculatorData calculatorData);
}
