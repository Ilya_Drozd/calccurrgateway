package org.clevertec.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CalcCurrGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalcCurrGatewayApplication.class, args);
	}

}
