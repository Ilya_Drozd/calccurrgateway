package org.clevertec.gateway.constants;

public class MessageNames {

    public static final String SUCCESSFUL_CALCULATOR_OPERATION_MESSAGE = "calculator.successfulOperationMessage";

    public static final String CONVERSION_AMOUNT_MESSAGE = "currency.conversionAmountMessage";
    public static final String CONVERSION_RATE_MESSAGE = "currency.conversionRateMessage";
    public static final String CULTURE_INFO_MESSAGE = "currency.cultureInfoMessage";
    public static final String CURRENCIES_MESSAGE = "currency.currenciesMessage";
    public static final String CURRENCY_RATE_MESSAGE = "currency.currencyRateMessage";
    public static final String LAST_UPDATE_DATE_MESSAGE = "currency.lastUpdateDateMessage";

    public final static String ERROR_INPUT_DATA_MESSAGE = "errors.errorInputDataMessage";
    public final static String ERROR_SERVER_MESSAGE = "errors.errorServerMessage";

}
