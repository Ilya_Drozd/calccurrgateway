package org.clevertec.gateway.repository;

import org.clevertec.gateway.dto.CalculatorData;

public interface CalculatorRepository {
    int calculate(CalculatorData calculatorData);
}
