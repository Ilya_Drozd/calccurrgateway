package org.clevertec.gateway.repository.impl;

import lombok.RequiredArgsConstructor;
import org.clevertec.gateway.configs.CalculatorProperties;
import org.clevertec.gateway.dto.CalculatorData;
import org.clevertec.gateway.repository.CalculatorRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
@RequiredArgsConstructor
public class CalculatorRepositoryImpl implements CalculatorRepository {

    private final RestTemplate restTemplate;
    private final CalculatorProperties calculatorProperties;

    @Override
    public int calculate(CalculatorData calculatorData) {
        HttpEntity<CalculatorData> requestBody = new HttpEntity<>(calculatorData);
        URI calculateUri = getCalculateUri();

        ResponseEntity<Integer> entity = restTemplate.postForEntity(calculateUri, requestBody, Integer.class);

        return entity.getBody();
    }

    private URI getCalculateUri() {
        return UriComponentsBuilder
                .fromUriString(String
                        .join("", calculatorProperties.getScheme(), calculatorProperties.getBaseInfo(),
                                calculatorProperties.getCalculatePath()))
                .encode()
                .build()
                .toUri();
    }

}
