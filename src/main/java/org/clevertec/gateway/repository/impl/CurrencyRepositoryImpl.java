package org.clevertec.gateway.repository.impl;

import lombok.RequiredArgsConstructor;
import org.clevertec.gateway.configs.CurrencyProperties;
import org.clevertec.gateway.dto.CurrencyConverterData;
import org.clevertec.gateway.dto.CurrencyData;
import org.clevertec.gateway.repository.CurrencyRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CurrencyRepositoryImpl implements CurrencyRepository {

    private final RestTemplate restTemplate;
    private final CurrencyProperties currencyProperties;

    @Override
    public BigDecimal getConversionAmount(CurrencyConverterData currencyConverterData) {
        HttpEntity<CurrencyConverterData> requestBody = new HttpEntity<>(currencyConverterData);

        URI conversionAmountUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getConversionAmountPath()))
                .encode()
                .build()
                .toUri();

        ResponseEntity<BigDecimal> entity = restTemplate.postForEntity(conversionAmountUri, requestBody,
                BigDecimal.class);

        return entity.getBody();
    }

    @Override
    public BigDecimal getConversionRate(CurrencyConverterData currencyConverterData) {
        HttpEntity<CurrencyConverterData> requestBody = new HttpEntity<>(currencyConverterData);

        URI conversionRateUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getConversionRatePath()))
                .encode()
                .build()
                .toUri();

        ResponseEntity<BigDecimal> entity = restTemplate.postForEntity(conversionRateUri, requestBody,
                BigDecimal.class);

        return entity.getBody();
    }

    @Override
    public String getCultureInfo(CurrencyData currencyData) {
        HttpEntity<CurrencyData> requestBody = new HttpEntity<>(currencyData);

        URI cultureInfoUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getCultureInfoPath()))
                .encode()
                .build()
                .toUri();

        ResponseEntity<String> entity = restTemplate.postForEntity(cultureInfoUri, requestBody, String.class);
        return entity.getBody();
    }

    @Override
    public List<String> getCurrencies() {
        URI currenciesUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getCurrenciesPath()))
                .encode()
                .build()
                .toUri();

        ResponseEntity<List> entity = restTemplate.getForEntity(currenciesUri, List.class);
        return (List<String>) entity.getBody();
    }

    @Override
    public BigDecimal getCurrencyRate(CurrencyData currencyData) {
        HttpEntity<CurrencyData> requestBody = new HttpEntity<>(currencyData);

        URI currencyRateUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getCurrencyRatePath()))
                .encode()
                .build()
                .toUri();

        ResponseEntity<BigDecimal> entity = restTemplate.postForEntity(currencyRateUri, requestBody, BigDecimal.class);
        return entity.getBody();
    }

    @Override
    public XMLGregorianCalendar getLastUpdateDate() {
        URI lastUpdateDateUri = UriComponentsBuilder
                .fromUriString(String
                        .join("", currencyProperties.getScheme(), currencyProperties.getBaseInfo(),
                                currencyProperties.getLastUpdateDatePath()))
                .encode()
                .build()
                .toUri();
        ResponseEntity<XMLGregorianCalendar> entity = restTemplate.getForEntity(lastUpdateDateUri,
                XMLGregorianCalendar.class);
        return entity.getBody();
    }
}
