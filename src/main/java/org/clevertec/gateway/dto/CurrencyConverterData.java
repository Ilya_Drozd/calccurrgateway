package org.clevertec.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyConverterData {
    private String currencyFrom;
    private String currencyTo;
    private String rateDate;
    private BigDecimal amount;
}
