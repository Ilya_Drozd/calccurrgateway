package org.clevertec.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculatorData {

    private double[] numbers = new double[2];

    private String operationType;

}
