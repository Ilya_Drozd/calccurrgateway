package org.clevertec.gateway.service;

import org.clevertec.gateway.dto.CurrencyConverterData;
import org.clevertec.gateway.dto.CurrencyData;
import org.clevertec.gateway.exception.BadInputDataException;
import org.clevertec.gateway.exception.ServerErrorException;
import org.clevertec.gateway.repository.CurrencyRepository;
import org.clevertec.gateway.service.CurrencyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyServiceImplTest {

    @MockBean
    private CurrencyRepository currencyRepository;

    @Autowired
    private CurrencyService currencyService;

    private CurrencyConverterData currencyConverterData = new CurrencyConverterData();
    private CurrencyData currencyData = new CurrencyData();

    @Test
    public void getConversionAmount() {
        BigDecimal conversionAmount = new BigDecimal("50");
        given(this.currencyRepository.getConversionAmount(currencyConverterData)).willReturn(conversionAmount);

        BigDecimal result = currencyService.getConversionAmount(currencyConverterData);
        Mockito.verify(currencyRepository).getConversionAmount(currencyConverterData);
        assertEquals(result, conversionAmount);
    }

    @Test(expected = ServerErrorException.class)
    public void getConversionAmountWithThrowingServerErrorException(){
        given(this.currencyRepository.getConversionAmount(currencyConverterData))
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getConversionAmount(currencyConverterData);
    }

    @Test(expected = BadInputDataException.class)
    public void getConversionAmountWithThrowingBadInputDataException(){
        given(this.currencyRepository.getConversionAmount(currencyConverterData))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad input data"));

        currencyService.getConversionAmount(currencyConverterData);
    }


    @Test
    public void getConversionRate() {
        BigDecimal conversionRate = new BigDecimal("50");
        given(this.currencyRepository.getConversionRate(currencyConverterData)).willReturn(conversionRate);

        BigDecimal result = currencyService.getConversionRate(currencyConverterData);
        Mockito.verify(currencyRepository).getConversionRate(currencyConverterData);
        assertEquals(result, conversionRate);
    }

    @Test(expected = ServerErrorException.class)
    public void getConversionRateWithThrowingServerErrorException(){
        given(this.currencyRepository.getConversionRate(currencyConverterData))
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getConversionRate(currencyConverterData);
    }

    @Test(expected = BadInputDataException.class)
    public void getConversionRateWithThrowingBadInputDataException(){
        given(this.currencyRepository.getConversionRate(currencyConverterData))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad input data"));

        currencyService.getConversionRate(currencyConverterData);
    }


    @Test
    public void getCultureInfo() {
        String cultureInfo = "sv-FI";
        given(this.currencyRepository.getCultureInfo(currencyData)).willReturn(cultureInfo);

        String result = currencyService.getCultureInfo(currencyData);
        Mockito.verify(currencyRepository).getCultureInfo(currencyData);
        assertEquals(result, cultureInfo);
    }

    @Test(expected = ServerErrorException.class)
    public void getCultureInfoWithThrowingServerErrorException(){
        given(this.currencyRepository.getCultureInfo(currencyData))
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getCultureInfo(currencyData);
    }

    @Test(expected = BadInputDataException.class)
    public void getCultureInfoWithThrowingBadInputDataException(){
        given(this.currencyRepository.getCultureInfo(currencyData))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad input data"));

        currencyService.getCultureInfo(currencyData);
    }


    @Test
    public void getCurrencies() {
        List<String> currencies = Arrays.asList("RUB", "EUR", "USD");
        given(this.currencyRepository.getCurrencies()).willReturn(currencies);

        List<String> result = currencyService.getCurrencies();
        Mockito.verify(currencyRepository).getCurrencies();
        assertEquals(result, currencies);
    }

    @Test(expected = ServerErrorException.class)
    public void getCurrenciesWithThrowingServerErrorException(){
        given(this.currencyRepository.getCurrencies())
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getCurrencies();
    }


    @Test
    public void getCurrencyRate() {
        BigDecimal currencyRate = new BigDecimal("50");
        given(this.currencyRepository.getCurrencyRate(currencyData)).willReturn(currencyRate);

        BigDecimal result = currencyService.getCurrencyRate(currencyData);
        Mockito.verify(currencyRepository).getCurrencyRate(currencyData);
        assertEquals(result, currencyRate);
    }

    @Test(expected = ServerErrorException.class)
    public void getCurrencyRateWithThrowingServerErrorException(){
        given(this.currencyRepository.getCurrencyRate(currencyData))
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getCurrencyRate(currencyData);
    }

    @Test(expected = BadInputDataException.class)
    public void getCurrencyRateWithThrowingBadInputDataException(){
        given(this.currencyRepository.getCurrencyRate(currencyData))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad input data"));

        currencyService.getCurrencyRate(currencyData);
    }


    @Test
    public void getLastUpdateDate() throws DatatypeConfigurationException {
        XMLGregorianCalendar date = DatatypeFactory.newInstance()
                .newXMLGregorianCalendar("2020-01-27T00:00:00");
        given(this.currencyRepository.getLastUpdateDate()).willReturn(date);

        XMLGregorianCalendar result = currencyService.getLastUpdateDate();
        Mockito.verify(currencyRepository).getLastUpdateDate();
        assertEquals(result, date);
    }

    @Test(expected = ServerErrorException.class)
    public void getLastUpdateDateWithThrowingServerErrorException(){
        given(this.currencyRepository.getLastUpdateDate())
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        currencyService.getLastUpdateDate();
    }
}