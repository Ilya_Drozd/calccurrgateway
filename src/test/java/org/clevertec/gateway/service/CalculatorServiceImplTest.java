package org.clevertec.gateway.service;

import org.clevertec.gateway.dto.CalculatorData;
import org.clevertec.gateway.exception.BadInputDataException;
import org.clevertec.gateway.exception.ServerErrorException;
import org.clevertec.gateway.repository.CalculatorRepository;
import org.clevertec.gateway.service.CalculatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorServiceImplTest {

    @MockBean
    private CalculatorRepository calculatorRepository;

    @Autowired
    private CalculatorService calculatorService;

    private int num1 = 5;
    private int num2 = 2;
    private String operation = "+";
    private CalculatorData calculatorData = new CalculatorData(new double[]{num1, num2}, operation);

    @Test
    public void calculate() {
        given(this.calculatorRepository.calculate(calculatorData)).willReturn(num1 + num2);

        int result = calculatorService.calculate(calculatorData);
        assertEquals(result, num1 + num2);
    }

    @Test(expected = ServerErrorException.class)
    public void calculateWithThrowingServerErrorException(){
        given(this.calculatorRepository.calculate(calculatorData))
                .willThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error"));

        calculatorService.calculate(calculatorData);
    }

    @Test(expected = BadInputDataException.class)
    public void calculateWithThrowingBadInputDataException(){
        given(this.calculatorRepository.calculate(calculatorData))
                .willThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad input data"));

        calculatorService.calculate(calculatorData);
    }
}