package org.clevertec.gateway.repository;

import org.clevertec.gateway.dto.CurrencyConverterData;
import org.clevertec.gateway.dto.CurrencyData;
import org.clevertec.gateway.repository.CurrencyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyRepositoryImplTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private CurrencyRepository currencyRepository;

    private CurrencyConverterData currencyConverterData = new CurrencyConverterData();
    private CurrencyData currencyData = new CurrencyData();

    @Test
    public void getConversionAmount() {
        given(this.restTemplate.postForEntity(any(URI.class), any(HttpEntity.class), eq(BigDecimal.class)))
                .willReturn(new ResponseEntity<>(new BigDecimal("50"), HttpStatus.OK));

        BigDecimal result = currencyRepository.getConversionAmount(currencyConverterData);
        assertEquals(result, new BigDecimal("50"));
    }

    @Test
    public void getConversionRate() {
        given(this.restTemplate.postForEntity(any(URI.class), any(HttpEntity.class), eq(BigDecimal.class)))
                .willReturn(new ResponseEntity<>(new BigDecimal("50"), HttpStatus.OK));

        BigDecimal result = currencyRepository.getConversionRate(currencyConverterData);
        assertEquals(result, new BigDecimal("50"));
    }

    @Test
    public void getCultureInfo() {
        String cultureInfo = "sv-FI";
        given(this.restTemplate.postForEntity(any(URI.class), any(HttpEntity.class), eq(String.class)))
                .willReturn(new ResponseEntity<>(cultureInfo, HttpStatus.OK));

        String result = currencyRepository.getCultureInfo(currencyData);
        assertEquals(result, cultureInfo);
    }

    @Test
    public void getCurrencies() {
        List<String> list = Arrays.asList("USD", "EUR", "RUB");
        given(this.restTemplate.getForEntity(any(URI.class), eq(List.class)))
                .willReturn(new ResponseEntity<>(list, HttpStatus.OK));
        List<String> result = currencyRepository.getCurrencies();
        assertEquals(result, list);
    }

    @Test
    public void getCurrencyRate() {
        given(this.restTemplate.postForEntity(any(URI.class), any(HttpEntity.class), eq(BigDecimal.class)))
                .willReturn(new ResponseEntity<>(new BigDecimal("50"), HttpStatus.OK));

        BigDecimal result = currencyRepository.getCurrencyRate(currencyData);
        assertEquals(result, new BigDecimal("50"));
    }

    @Test
    public void getLastUpdateDate() throws DatatypeConfigurationException {
        XMLGregorianCalendar date = DatatypeFactory.newInstance()
                .newXMLGregorianCalendar("2020-01-27T00:00:00");
        given(this.restTemplate.getForEntity(any(URI.class), eq(XMLGregorianCalendar.class)))
                .willReturn(new ResponseEntity<>(date, HttpStatus.OK));

        XMLGregorianCalendar result = currencyRepository.getLastUpdateDate();
        assertEquals(date, result);
    }
}