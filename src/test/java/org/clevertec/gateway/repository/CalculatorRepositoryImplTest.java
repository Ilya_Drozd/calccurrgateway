package org.clevertec.gateway.repository;

import org.clevertec.gateway.configs.CalculatorProperties;
import org.clevertec.gateway.dto.CalculatorData;
import org.clevertec.gateway.repository.CalculatorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorRepositoryImplTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private CalculatorRepository calculatorRepository;

    @Autowired
    private CalculatorProperties calculatorProperties;


    private CalculatorData calculatorData;
    private HttpEntity<CalculatorData> requestBody;
    private ResponseEntity<Integer> entity;

    private URI url;
    private int num1;
    private int num2;

    @Before
    public void init(){
        num1 = 2;
        num2 = 3;
        String operation = "+";

        calculatorData = new CalculatorData(new double[]{num1, num2}, operation);
        requestBody = new HttpEntity<>(calculatorData);
        entity = new ResponseEntity<>(num1 + num2, HttpStatus.OK);
    }

    @Before
    public void createUrl(){
       url = UriComponentsBuilder
                .fromUriString(String
                        .join("", calculatorProperties.getScheme(), calculatorProperties.getBaseInfo(),
                                calculatorProperties.getCalculatePath()))
                .encode()
                .build()
                .toUri();
    }

    @Test
    public void calculate() {
        given(this.restTemplate.postForEntity(url, requestBody, Integer.class)).willReturn(entity);

        int result = calculatorRepository.calculate(calculatorData);

        Mockito.verify(restTemplate).postForEntity(url, requestBody, Integer.class);
        assertEquals(num1 + num2, result);
    }
}