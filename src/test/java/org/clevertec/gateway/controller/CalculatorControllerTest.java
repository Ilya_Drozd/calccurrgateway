package org.clevertec.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.clevertec.gateway.dto.CalculatorData;
import org.clevertec.gateway.service.CalculatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@ComponentScan("org.clevertec.gateway")
@AutoConfigureWebClient
public class CalculatorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CalculatorService calculatorService;

    private final CalculatorData calculatorData = new CalculatorData(new double[]{2, 3}, "+");

    @Test
    public void calculate() throws Exception {
        given(this.calculatorService.calculate(calculatorData)).willReturn(5);

        this.mockMvc.perform(post("http://localhost:8099/calculator/calculate")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(calculatorData)))
                .andExpect(status().isOk());
    }
}