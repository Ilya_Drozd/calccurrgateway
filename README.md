# Gateway for Calculator and CurrencyConverter

## Description
REST service for working with REST calculator and REST converter

## Calculator: 
    
#### Calculate
#### POST [http://localhost:8099/calculator/calculate](http://localhost:8099/calculator/calculate)
#### calculating (addition, subtraction, multiplication and division) of two numbers

        Example of request body:
        {
            "numbers": [ 3, 4 ],
        	"operationType": "+"
        }

---

## CurrencyConverter:
    
#### Conversion amount
#### POST [http://localhost:8099/currencies/conversion/amount](http://localhost:8099/currencies/conversion/amount)
#### getting conversion amount of two currencies

        Example of request body:
        {
            "currencyFrom": "USD",
        	"currencyTo": "RUB",
        	"rateDate": "2020-01-27T00:00:00",
        	"amount": "10"
        }

---

#### Conversion rate
#### POST [http://localhost:8099/currencies/conversion/rate](http://localhost:8099/currencies/conversion/rate)
#### getting conversion rate of two currencies

        Example of request body:
        {
            "currencyFrom": "USD",
        	"currencyTo": "RUB",
        	"rateDate": "2020-01-27T00:00:00"
        }

---

#### Culture info
#### POST [http://localhost:8099/currencies/currency/info](http://localhost:8099/currencies/currency/info)
#### getting culture info of currency

        Example of request body:
        {
        	"currency": "USD"
        }

---

#### Get currencies
#### GET [http://localhost:8099/currencies](http://localhost:8099/currencies)
#### receiving all currencies

---

#### Currency rate
#### POST [http://localhost:8099/currencies/currency/rate](http://localhost:8099/currencies/currency/rate)
#### getting rate of currency

        Example of request body:
        {
        	"currency": "RUB",
        	"rateDate": "2020-01-27T00:00:00"
        }

---

#### Last update date
#### GET [http://localhost:8099/currencies/date](http://localhost:8099/currencies/date)
#### receiving last update date 

---